#!/bin/bash
kubectl delete -f deploy/crds/lab_v1_jenkins_cr.yaml
kubectl delete -f deploy/operator.yaml
kubectl delete -f deploy/role_binding.yaml
kubectl delete -f deploy/role.yaml
kubectl delete -f deploy/service_account.yaml
kubectl delete -f deploy/crds/lab_v1_jenkins_crd.yaml
kubectl delete -f deploy/olm-catalog/jenkins-operator/0.0.3/jenkins-operator.v0.0.3.clusterserviceversion.yaml
