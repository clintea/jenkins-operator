#!/bin/bash
kubectl create -f deploy/crds/lab_v1_jenkins_crd.yaml
operator-sdk --image-builder=buildah build registry.gitlab.com/clintea/jenkins-operator:v1
podman login -u${GITLAB_USER} -p${GITLAB_PASSWORD} registry.gitlab.com
podman push registry.gitlab.com/clintea/jenkins-operator:v1
oc create -f roles/jenkins/templates/rb.yaml
kubectl create -f deploy/service_account.yaml
kubectl create -f deploy/role.yaml
kubectl create -f deploy/role_binding.yaml
kubectl create -f deploy/operator.yaml
oc apply -f deploy/olm-catalog/jenkins-operator/0.0.3/jenkins-operator.v0.0.3.clusterserviceversion.yaml
# kubectl apply -f deploy/crds/lab_v1_jenkins_cr.yaml
